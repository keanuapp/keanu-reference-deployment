# ---------------------------------------------------------------------------------------------------------------------
# TERRAGRUNT CONFIGURATION
# Terragrunt is a thin wrapper for Terraform that provides extra tools for working with multiple Terraform modules,
# remote state, and locking: https://github.com/gruntwork-io/terragrunt
# ---------------------------------------------------------------------------------------------------------------------

locals {
  project_vars = read_terragrunt_config(find_in_parent_folders("project.hcl"))
  region_vars  = read_terragrunt_config(find_in_parent_folders("region.hcl"))

  account_name = local.project_vars.locals.account_name
  account_id   = local.project_vars.locals.aws_account_id
  environment  = local.project_vars.locals.environment
  namespace    = local.project_vars.locals.namespace
  project      = local.project_vars.locals.project
  aws_region   = local.region_vars.locals.aws_region
}

# Generate an AWS provider block
generate "provider" {
  path      = "provider.tf"
  if_exists = "overwrite_terragrunt"
  contents  = <<EOF
provider "aws" {
  region = "${local.aws_region}"

  # Only these AWS Account IDs may be operated on by this template
  allowed_account_ids = ["${local.account_id}"]
}
EOF
}

## Configure Terragrunt to automatically store tfstate files in an S3 bucket
remote_state {
  backend           = "s3"
  config            = {
    encrypt         = true
    bucket          = "${local.namespace}-${local.environment}-terraform-state"
    key             = "${path_relative_to_include()}/terraform.tfstate"
    region          = local.aws_region
    dynamodb_table  = "${local.namespace}-${local.environment}-terraform-locks"
    s3_bucket_tags  = {
      "Namespace"   = local.namespace
      "Environment" = local.environment
      "Project"     = local.project
    }
  }
  generate = {
    path      = "backend.tf"
    if_exists = "overwrite_terragrunt"
  }
}


# ---------------------------------------------------------------------------------------------------------------------
# GLOBAL INPUT VARIALBES
# These variables apply to all configurations in this subfolder. These are automatically merged into the child
# `terragrunt.hcl` config via the include block.
# ---------------------------------------------------------------------------------------------------------------------
inputs = {
  namespace    = local.namespace
  environment  = local.environment
  project      = local.project
  tags         = local.project_vars.locals.tags
  aws_region   = local.aws_region
  acccount_id  = local.account_id
  is_prod_like = local.project_vars.locals.is_prod_like
}
