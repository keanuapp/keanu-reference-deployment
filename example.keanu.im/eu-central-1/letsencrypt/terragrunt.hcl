# Include all settings from the root terragrunt.hcl file
locals {
  name    = "letsencrypt"
  project = read_terragrunt_config(find_in_parent_folders("project.hcl"))
  commonx = yamldecode(sops_decrypt_file("${local.project.locals.data_dir}/common.sops.yml"))
  tls     = yamldecode(sops_decrypt_file("${local.project.locals.data_dir}/tls.sops.yml"))

  matrix_dns_names   = split(",", local.commonx.matrix_dns_names)
  matrix_common_name = local.commonx.matrix_common_name
  riot_dns_names     = split(",", local.commonx.riot_dns_names)
  riot_common_name   = local.commonx.riot_common_name

}

terraform {
  source = "git::https://gitlab.com/guardianproject-ops/terraform-modules-keanu//letsencrypt?ref=master"
}

include {
  path = find_in_parent_folders()
}

inputs = {
  name = local.name

  email_address = local.tls.acme_email

  cloudflare_dns_api_token  = local.commonx.cloudflare_dns_api_token
  cloudflare_zone_api_token = local.commonx.cloudflare_zone_api_token

  certs = {
    "matrix" = {
      common_name          = local.matrix_common_name
      dns_names            = local.matrix_dns_names
      tls_private_key      = local.tls.matrix.key
      tls_private_key_algo = local.tls.matrix.algo
      force_renewal        = false
    }
    "client" = {
      common_name          = local.riot_common_name
      dns_names            = local.riot_dns_names
      tls_private_key      = local.tls.riot.key
      tls_private_key_algo = local.tls.riot.algo
      force_renewal        = false
    }
  }
}
